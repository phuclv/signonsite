<?php
/**
 * Created by PhpStorm.
 * User: PhucLam
 * Date: 6/5/18
 * Time: 2:18 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'companies';
    protected $fillable = ['name'];

    public $timestamps = false;

    public function equipment()
    {
        return $this->hasMany('App\Models\Equipment', 'company_id');
    }
}