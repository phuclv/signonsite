<?php

namespace App\Http\Controllers\Api;

use App\Models\Equipment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class EquipmentController extends Controller
{
    public function index()
    {
        $records = Equipment::all()->toArray();

        return response()->json(['success' => true, 'data' => $records]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'date_registered' => 'nullable|date_format:"Y-m-d"',
            'last_serviced' => 'nullable|date_format:"Y-m-d"'
        ]);

        if($validator->fails()) {
            return response()->json(['success' => false, 'errors' => $validator->errors()], 400);
        }

        Equipment::create($request->all());

        return response()->json(['success' => true]);
    }
}
