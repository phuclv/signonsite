<?php

namespace App\Http\Controllers;

use App\Models\Equipment;
use Illuminate\Http\Request;

class EquipmentController extends Controller
{
    public function index()
    {
        return view('equipment.index');
    }

    public function create()
    {
        return view('equipment.form');
    }

    public function export()
    {
        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=equipments.csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        $equipments = Equipment::with('user')->with('company')->get();

        $columns = array(
            'Name',
            'Brand/Model',
            'Vehicle Class',
            'Owning company',
            'Person Responsible',
            'Serial Number',
            'Operation',
            'Performance',
            'Registered on site',
            'Last serviced');

        $callback = function () use ($equipments, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach ($equipments as $v) {
                fputcsv($file, array(
                        $v->name,
                        $v->model,
                        $v->vehicle_lass,
                        !empty($v->company) ? $v->company->name : '',
                        !empty($v->user) ? $v->user->name : '',
                        $v->serial_number,
                        $v->operation ? 'Single' : '',
                        $v->performance_number . $v->performance_unit,
                        !empty($v->date_registered) ? date('j/n/Y', strtotime($v->date_registered)) : '',
                        !empty($v->last_serviced) ? date('j/n/Y', strtotime($v->last_serviced)) : '',
                    )
                );
            }
            fclose($file);
        };
        return response()->stream($callback, 200, $headers);
    }
}
