## Installation
Run these commands in below:

 - `composer update`
 - `php artisan migrate --seed`
 - `npm install`
 - `npm run dev`
