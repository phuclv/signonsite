<?php

use Illuminate\Database\Seeder;

class EquipmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('equipments')->insert([
            [
                'name' => 'Forklift',
                'model' => 'CAT Forklift',
                'vehicle_lass' => '3C',
                'serial_number' => '123123',
                'operation' => 1,
                'performance_number' => '100',
                'performance_unit' => 'kg',
                'date_registered' => '2018-05-14',
                'last_serviced' => '2018-04-21',
            ],
            [
                'name' => 'Big Crane',
                'model' => 'CAT Crane',
                'vehicle_lass' => 'F',
                'serial_number' => '234234ABA',
                'operation' => 1,
                'performance_number' => 1,
                'performance_unit' => 'tonne',
                'date_registered' => '2018-02-02',
                'last_serviced' => '2018-03-14',
            ],
            [
                'name' => 'Generator',
                'model' => 'Super Generator 9000',
                'vehicle_lass' => '',
                'serial_number' => '456456LS',
                'operation' => 0,
                'performance_number' => 40,
                'performance_unit' => 'MW',
                'date_registered' => '2018-01-01',
                'last_serviced' => '2018-01-02',
            ]
        ]);
    }
}
