<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'Krishan Caldwell',
                'email' => str_random(10).'@gmail.com',
                'password' => bcrypt('secret'),
            ],
            [
                'name' => 'Matt Thomas Whittaker',
                'email' => str_random(10).'@gmail.com',
                'password' => bcrypt('secret'),
            ],
            [
                'name' => 'James Long',
                'email' => str_random(10).'@gmail.com',
                'password' => bcrypt('secret'),
            ],
            [
                'name' => 'Michael Jackson',
                'email' => str_random(10).'@gmail.com',
                'password' => bcrypt('secret'),
            ],
            [
                'name' => 'Hank Hill',
                'email' => str_random(10).'@gmail.com',
                'password' => bcrypt('secret'),
            ],
        ]);
    }
}
