@extends('layout')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <equipment-form></equipment-form>
            </div>
        </div>
    </div>
@endsection