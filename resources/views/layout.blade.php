<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PHP Assignment</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{ mix('css/app.css') }}" rel="stylesheet" type="text/css">
</head>
<body>
<div id="app">
    <div class="container">
        <div class="header">
            <nav>
                <ul class="nav nav-pills pull-right">
                    <li class="active"><a href="{{ route('index') }}">Index</a></li>
                    <li><a href="{{ route('create') }}">Create</a></li>
                    <li><a href="{{ route('export') }}" target="_blank">Export CSV</a></li>
                </ul>
            </nav>
        </div>
    </div>
    @yield('content')
</div>
<script src="{{ mix('js/app.js') }}"></script>
</body>
</html>